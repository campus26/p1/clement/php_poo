<?php

class StudentModel{
    
    public function index($dbConnection)
{
    $query = 'SELECT id, last_name, first_name FROM student';
	$statement = $dbConnection->prepare($query);
	$statement->execute();
	$students = $statement->fetchAll(PDO::FETCH_ASSOC);

    $pageName = 'Students';

    include_once(TEMPLATES .'student/index.php');
}

    public function findById($dbConnection, $studentId)
{
    $query = 'SELECT id, last_name, first_name, date_of_birth, address FROM student WHERE id = :studentId';
	$statement = $dbConnection->prepare($query);
    $statement->bindParam(':studentId', $studentId);
	$statement->execute();
    
    return $statement->fetch(PDO::FETCH_ASSOC);    
}


public function add($dbConnection)
{
    $lastName = postParam('lastName');
    $firstName = postParam('firstName');
    $dateOfBirth = postParam('dateOfBirth');
    $address = postParam('address');

    $query = 'INSERT INTO student(last_name, first_name, date_of_birth, address) VALUES(:lastName, :firstName, :dateOfBirth, :address)';
	$statement = $dbConnection->prepare($query);
    $statement->bindParam(':lastName', $lastName);
    $statement->bindParam(':firstName', $firstName);
    $statement->bindParam(':dateOfBirth', $dateOfBirth);
    $statement->bindParam(':address', $address);
	$statement->execute();
    $studentId = $dbConnection->lastInsertId();
    
    echo json_encode(['studentId' => $studentId]);
}


public function update($dbConnection)
{
    $studentId = postParam('studentId');
    $lastName = postParam('lastName');
    $firstName = postParam('firstName');
    $dateOfBirth = postParam('dateOfBirth');
    $address = postParam('address');

    $query = 'UPDATE student SET last_name = :lastName, first_name = :firstName, date_of_birth = :dateOfBirth, address = :address WHERE id = :studentId';
	$statement = $dbConnection->prepare($query);
    $statement->bindParam(':studentId', $studentId, PDO::PARAM_INT);
    $statement->bindParam(':lastName', $lastName);
    $statement->bindParam(':firstName', $firstName);
    $statement->bindParam(':dateOfBirth', $dateOfBirth);
    $statement->bindParam(':address', $address);
    $statement->execute();
    $rowCount = $statement->rowCount();

    echo json_encode(['rowCount' => $rowCount]);
}


public function delete($dbConnection)
{
    $studentId = getParam('studentId');

    $query = "DELETE FROM student WHERE id =:studentId";
    $statement = $dbConnection->prepare($query);
    $statement->bindParam(':studentId', $studentId);
    $statement->execute();
    $rowCount = $statement->rowCount();

    echo json_encode(['rowCount' => $rowCount]);
}

}