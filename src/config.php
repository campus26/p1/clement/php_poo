<?php

define('APPLICATION_NAME', 'Procedural PHP');

// Database
define('DB_VENDOR', 'mysql');
define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_NAME', 'proceduralphp');
define('DB_CHARSET', 'utf-8');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

define('DEFAULT_PAGE', 'home');
define('DEFAULT_ACTION', 'index');
define('PAGE', __DIR__ . '/../src/pages/');
define('TEMPLATES', __DIR__ . '/../templates/');

// Routes
$routes = [
    DEFAULT_PAGE => [ 
        'controleur' =>'Home',
        DEFAULT_ACTION
    ],
    
    'student' => [
        'controleur' =>'Student',
        DEFAULT_ACTION,
        'add',
        'update',
        'edit',
        'view',
        'delete'
    ]
];