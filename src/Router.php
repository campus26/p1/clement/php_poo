<?php 
   

class Router
{
    private $page;
    private $action;
    function __construct() {

        $this->page = getParam('p');
        $this->action = getParam('a');
        
    
        if ($this->page === NULL)
        {
            $this->page = DEFAULT_PAGE;
        }
    
        if ($this->action === NULL)
        {
            $this->action = DEFAULT_ACTION;
            
        }
    }


    private function doGet($routes, $dbConnection)
    {
      
        
        if (array_key_exists($this->page, $routes)) {
            $matched = false;
            foreach($routes[$this->page] as $value)
            {
                if ($this->action === $value)
                {
                    
                    $matched = true;
                    break;                
                } 
            }
    
            if ($matched)
            {
                $pageFile = PAGE . $this->page . '.php';
                if (file_exists($pageFile))
                {
                    $MonController=new $routes[$this->page]['controleur'];
              
                 $MonController->{$this->action}($dbConnection);
                    
                  
                }
            } else {
                echo 'Error 404<br>Page not found';
            }
            
        } else {
            echo 'Error 404<br>Page not found';
        }
    }


    private function doPost($routes, $dbConnection)
    {
        
        
        if (array_key_exists($this->page, $routes)) {
            $matched = false;
            foreach($routes[$this->page] as $value)
            {
                if ($this->action === $value)
                {
                    
                    $matched = true;
                    break;                
                } 
            }
    
            if ($matched)
            {
                $pageFile = PAGE . $this->page . '.php';
                if (file_exists($pageFile))
                {
                    $MonController=new $routes[$this->page]['controleur'];
                    $MonController->{$this->action}($dbConnection);
                }
            } else {
                echo 'Error 404<br>Page not found';
            }
            
        } else {
            echo 'Error 404<br>Page not found';
        }
    }
    

    public function init($routes, $dbConnection)
    {
        $httpMethod = httpMethod();
        
        switch ($httpMethod) {
            case 'GET':
                $this->doGet($routes, $dbConnection);
                break;
    
            case 'POST':
                $this->doPost($routes, $dbConnection);
                break;
    
            default:
                break;
        }
    }
    


    
  
}

?> 